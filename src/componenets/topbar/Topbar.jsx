import "./topbar.css";
import {
  Home,
  ListAlt,
  DriveFileRenameOutline,
  GroupsOutlined,
  NotificationsOutlined,
  Search,
  Language,
  KeyboardArrowDownOutlined,
} from "@mui/icons-material";
import { useState } from "react";

const Topbar = () => {
  const [active, setActive] = useState({
    home: false,
    list: false,
    drive: false,
    group: false,
    notification: false,
  });

  return (
    <header className="header">
      <div className="leftMarginBar"></div>
      <ul className="headerLinks">
        <li className="headerLink">
          <span className="headerLogo">Quora</span>
        </li>

        <li
          className={`headerIconLink ${active.home ? "active" : ""}`}
          onClick={() => {
            setActive({
              ...active,
              home: true,
              list: false,
              drive: false,
              group: false,
              notification: false,
            });
          }}>
          <Home
            className="icon first"
            htmlColor={active.home ? "#a62100" : "rgb(150, 147, 147)"}
            style={{ width: "28px", height: "28px" }}
          />
        </li>
        <li
          className={`headerIconLink ${active.list ? "active" : ""}`}
          onClick={() => {
            setActive({
              ...active,
              list: true,
              home: false,
              drive: false,
              group: false,
              notification: false,
            });
          }}>
          <ListAlt
            className="icon"
            htmlColor={active.list ? "#a62100" : "rgb(150, 147, 147)"}
            style={{ width: "28px", height: "28px" }}
          />
        </li>
        <li
          className={`headerIconLink ${active.drive ? "active" : ""}`}
          onClick={() => {
            setActive({
              ...active,
              drive: true,
              list: false,
              home: false,
              group: false,
              notification: false,
            });
          }}>
          <DriveFileRenameOutline
            htmlColor={active.drive ? "#a62100" : "rgb(150, 147, 147)"}
            className="icon"
            style={{ width: "28px", height: "28px" }}
          />
        </li>
        <li
          className={`headerIconLink ${active.group ? "active" : ""}`}
          onClick={() => {
            setActive({
              ...active,
              group: true,
              list: false,
              drive: false,
              home: false,
              notification: false,
            });
          }}>
          <GroupsOutlined
            htmlColor={active.group ? "#a62100" : "rgb(150, 147, 147)"}
            className="icon"
            style={{ width: "28px", height: "28px" }}
          />
        </li>
        <li
          className={`headerIconLink ${active.notification ? "active" : ""}`}
          onClick={() => {
            setActive({
              ...active,
              notification: true,
              list: false,
              drive: false,
              group: false,
              home: false,
            });
          }}>
          <NotificationsOutlined
            htmlColor={active.notification ? "#a62100" : "rgb(150, 147, 147)"}
            className="icon"
            style={{ width: "28px", height: "28px" }}
          />
        </li>

        <li className="headerSearchLink">
          <Search htmlColor="rgb(223, 213, 213)" className="searchIcon" />
          <input
            type="text"
            placeholder="Search Quora"
            className="searchInput"
          />
        </li>
        <li className="headerRightLinkTry">
          <button className="tryQuoraButton">Try Quora+</button>
        </li>
        <li className="headerRightLink">
          <img src="assets/1.jpeg" alt="" className="userLogo" />
        </li>
        <li className="headerRightLink">
          <Language
            htmlColor="rgb(150, 147, 147)"
            className="icon"
            style={{ width: "28px", height: "28px" }}
          />
        </li>
        <li className="headerRightQuestionLink">
          <button className="addQuestionButton">
            Add Question
            <KeyboardArrowDownOutlined
              style={{ borderLeft: "0.5px solid gray" }}
            />
          </button>
          <button></button>
        </li>
      </ul>
      <div className="rightMarginBar"></div>
    </header>
  );
};

export default Topbar;
