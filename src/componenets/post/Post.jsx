import "./post.css";
import {
  DownloadOutlined,
  MoreHorizOutlined,
  CloseOutlined,
  PublishOutlined,
  ChatBubbleOutlineOutlined,
  CachedOutlined,
} from "@mui/icons-material";

const Post = () => {
  return (
    <div className="post">
      <div className="postWrapper">
        <div className="postTop">
          <div className="postTopLeft">
            <img className="postTopLogo" src="assets/2.jpeg" alt="" />
            <div className="postTopLeftCont">
              <div className="postTopHeading">
                <span className="postTopHeadingName">Memes are Life</span>
                <span className="postTopHeadingSubscribe">Subscribe</span>
              </div>
              <div className="postTopPostedBy">
                <span className="postedByName">Posted by Deepak Yadav</span>
                <span className="postedByDate">Sat</span>
              </div>
            </div>
          </div>
          <div className="postTopRight">
            <div className="postTopRightMore">
              <MoreHorizOutlined className="postTopRightMoreIcon" />
            </div>
            <div className="postTopRightClose">
              <CloseOutlined className="postTopRightCloseIcon" />
            </div>
          </div>
        </div>
        <div className="postMiddle">
          <span className="postMiddleDesc">
            This is the first Post for the quora feed
          </span>

          <img className="postMiddleImg" src="assets/posts/1.jpeg" alt="" />
          <div className="postMiddleBottom">
            <span className="postMiddleBottomItem View">986 views .</span>
            <span className="postMiddleBottomItem Upvote">
              View 43 upvotes.
            </span>
            <span className="postMiddleBottomItem Accepted">
              Submission Accepted by Shiv Shankar Prasad
            </span>
          </div>
        </div>
        <div className="postBottom">
          <div className="postBottomVote">
            <PublishOutlined className="postBottomUpvote" />
            <span className="upvoteNumber">43</span>
            <DownloadOutlined className="postBottomDownvote" />
          </div>
          <div className="postBottomComment">
            <ChatBubbleOutlineOutlined className="postBottomCommentIcon" />
          </div>
          <div className="postBottomRefresh">
            <CachedOutlined className="postBottomRefreshIcon" />
          </div>
        </div>
        <div className="postComments">
          <img src="assets/1.jpeg" className="postCommentIcon" alt="" />
          <input
            type="text"
            className="commentInput"
            placeholder="Add a comment..."
          />
          <button className="commentButton">Add comment</button>
        </div>
      </div>
    </div>
  );
};

export default Post;
