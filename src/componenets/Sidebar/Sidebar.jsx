import "./sidebar.css";
import { AddOutlined } from "@mui/icons-material";

const Sidebar = () => {
  return (
    <div className="sidebar">
      <div className="sidebarWrpper">
        <div className="sidebarLink">
          <button className="sidebarCreateButton">
            <AddOutlined
              htmlColor="rgb(179, 176, 176)"
              style={{
                width: "16px",
                height: "16px",
                backgroundColor: "#e2dddd",
              }}
            />
            Create Space
          </button>
        </div>
        <ul className="sidebarLinks">
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Republic of memers</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Memes For Life</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Death Note</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Modern Day Traders</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Informative Diaries</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Silly Memes</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> itsindianguy</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Sad quote everyday</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> How to use Your brain</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span>Quora Creator hub</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> BeautiulGirlsPi</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Fun Discussions</span>
            </div>
          </li>
          <li className="sidebarLinkItem">
            <div className="sidebarLinkItemInner">
              <img className="sidebarLinkItemLogo" src="assets/3.jpeg" alt="" />
              <span> Discover Spaces</span>
            </div>
          </li>
        </ul>
        <div className="sidebarHrDiv">
          <hr className="sidebarHr" />
        </div>
        <ul className="sidebarFooterLinks">
          <li className="sidebarFooterLinkItem">About </li>.
          <li className="sidebarFooterLinkItem">Careers</li>.
          <li className="sidebarFooterLinkItem">Terms</li>.
          <li className="sidebarFooterLinkItem">Privacy</li>.
          <li className="sidebarFooterLinkItem">Acceptable Use</li>.
          <li className="sidebarFooterLinkItem">Business</li>.
          <li className="sidebarFooterLinkItem">Press</li>.
          <li className="sidebarFooterLinkItem">Your Ad choice</li>.
          <li className="sidebarFooterLinkItem">Grievance Officer</li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
