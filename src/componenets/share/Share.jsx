import "./share.css";
import {
  ModeEditOutlined,
  HelpCenterOutlined,
  DriveFileRenameOutlineSharp,
} from "@mui/icons-material";

const Share = () => {
  return (
    <div className="shareContainer">
      <div className="shareWrapper">
        <div className="shareTop">
          <img className="shareTopImg" src="assets/1.jpeg" alt="" />
          <button className="shareTopInput">
            What do you want to ask or share ?
          </button>
        </div>
        <div className="shareBottom">
          <button className="shareBottomButton">
            <HelpCenterOutlined className="shareButtonIcon" />
            Ask
          </button>
          |
          <button className="shareBottomButton">
            <DriveFileRenameOutlineSharp className="shareButtonIcon" />
            Answer
          </button>
          |
          <button className="shareBottomButton">
            <ModeEditOutlined className="shareButtonIcon" />
            Post
          </button>
        </div>
      </div>
    </div>
  );
};

export default Share;
