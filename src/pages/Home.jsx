import { Fragment } from "react";
import Feed from "../componenets/feed/Feed";
import Sidebar from "../componenets/Sidebar/Sidebar";
import Rightbar from "../componenets/rightbar/Rightbar";
import Topbar from "../componenets/topbar/Topbar";
import "./home.css";

const Home = () => {
  return (
    <Fragment>
      <Topbar />
      <main className="homeContainer">
        <div className="homeWrapper">
          <Sidebar />
          <Feed />
          <Rightbar />
        </div>
      </main>
    </Fragment>
  );
};

export default Home;
